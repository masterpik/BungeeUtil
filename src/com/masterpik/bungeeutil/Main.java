package com.masterpik.bungeeutil;

import com.masterpik.api.skype.SkypeManager;
import com.masterpik.bungeeconnect.database.UUIDName;
import com.masterpik.bungeeutil.commands.AdminSounds;
import com.masterpik.bungeeutil.commands.Reload;
import com.masterpik.bungeeutil.confy.GeneralConfy;
import com.masterpik.messages.bungee.Api;
import com.samczsun.skype4j.chat.Chat;
import com.samczsun.skype4j.events.EventHandler;
import com.samczsun.skype4j.events.Listener;
import com.samczsun.skype4j.events.chat.message.MessageReceivedEvent;
import com.samczsun.skype4j.exceptions.ChatNotFoundException;
import com.samczsun.skype4j.exceptions.ConnectionException;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.plugin.Plugin;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class Main extends Plugin {

    public static Plugin plugin;

    public static String outDateKick;

    public static Chat chat;

    public void onEnable() {

        plugin = this;

        ProxyServer.getInstance().getPluginManager().registerCommand(this, new Reload("bureload"));

        AdminSounds.adminSoundsInit();

        this.getProxy().getPluginManager().registerListener(this, new Events());

        GeneralConfy.GeneralInit();

        TabListManager.startTabMovement();

        Util.blockLogFromServerIp(this.getLogger());

        outDateKick = Api.getString("general.masterpik.main.logoText")+"\n \n"+Api.getString("proxy.errors.outDate");
        outDateKick = outDateKick.replaceAll("%v", GeneralConfy.getProtocolName());



        ProxyServer.getInstance().getScheduler().runAsync(this, new Runnable() {
            @Override
            public void run() {
                try {
                    chat = SkypeManager.getSkype().getOrLoadChat("19:b127886b53a54e51bf6dc9360fdbb61a@thread.skype");
                } catch (ConnectionException | ChatNotFoundException e) {
                    e.printStackTrace();
                }


                SkypeManager.getSkype().getEventDispatcher().registerListener(new Listener() {
                    @EventHandler
                    public void MessageReceivedEvent(MessageReceivedEvent event) {
                        String msg = event.getMessage().getContent().asPlaintext();

                        try {

                            if (msg.equalsIgnoreCase("drogue")) {
                                event.getChat().sendMessage("https://youtu.be/ZMDpo0eZ2IE");
                            } else if (msg.equalsIgnoreCase("trailer")) {
                                event.getChat().sendMessage("https://youtu.be/g7F0lKoS_gE");
                            } else if (msg.equalsIgnoreCase("teaser")) {
                                event.getChat().sendMessage("https://youtu.be/UEo_e4HmKwU");
                            } else if (msg.equalsIgnoreCase("youtube")) {
                                event.getChat().sendMessage("https://www.youtube.com/channel/UCm_VPFPuPkCd6Mo1YyKgqzA");
                            } else if (msg.equalsIgnoreCase("web") || msg.equalsIgnoreCase("site")) {
                                event.getChat().sendMessage("http://masterpik.com");
                            } else if (msg.equalsIgnoreCase("bug")) {
                                event.getChat().sendMessage("http://support.masterpik.com");
                            } else if (msg.equalsIgnoreCase("panel")) {
                                event.getChat().sendMessage("http://panel.masterpik.com");
                            } else if (msg.equalsIgnoreCase("mail")) {
                                event.getChat().sendMessage("mail (click) : mailto:mail@masterpik.com");
                            } else if (msg.equalsIgnoreCase("tfq") || msg.equalsIgnoreCase("tfk") || msg.equalsIgnoreCase("tu fais quoi") || msg.equalsIgnoreCase("tfq ?") || msg.equalsIgnoreCase("tfk ?") || msg.equalsIgnoreCase("tu fais quoi ?")) {
                                event.getChat().sendMessage("ba comment dire ... hum hum ... (party)");
                            } else if (msg.equalsIgnoreCase("salut")) {
                                event.getChat().sendMessage("Jean Claude Vandame");
                            } else if (msg.equalsIgnoreCase("slt")) {
                                event.getChat().sendMessage("Bonjour je m'appelle Sylvain Pierre Durif, j'ai coiffé les plus grands ...");
                            } else if (msg.equalsIgnoreCase("bonjour")) {
                                event.getChat().sendMessage("Bonjour monsieur fidèle, le nom de mon père c'est le nom du papa du savant de la république Eddy Malou, renaissance africaine comparé à réna");
                            } else if (msg.equalsIgnoreCase("Vous vous appelez comment ?")) {
                                event.getChat().sendMessage("Eddy Malou, le premier savant de toute la république démocratique du congo, EDDY MALOU, E-double-d-Y-Trait d'union-M-a-l-o-u, c'est à dire ça veut dire... Imposer la force veeers, le valium, ça veut dire l'estime du savoir, les gens qui connaissent beaucoup de choses et cristaliser, imposer, iiiiinnnnntentionner dans toute la république démocratique du congo pour que nous puissions avoir la congo-lexicomatisation des lois du marché propre aux congolais.");
                            } else if (msg.equalsIgnoreCase("parle")) {
                                event.getChat().sendMessage("Vous savez, moi je ne crois pas qu'il y ait de bonnes ou de mauvaises situations. Moi si je devais résumer ma vie, aujourd'hui, avec vous, je dirais que c'est d'abord des rencontres, des gens qui m'ont tendu la main, peut-être à un moment où je ne pouvais pas, où j'étais seul chez moi, et c'est assez curieux de se dire que les hasards, les rencontres forgent une destinée, parce que quand on a le goût de la chose, quand on a le goût de la chose bien faite, le beau geste, parfois on ne trouve pas l'interlocuteur en face, je dirais le miroir qui vous aide à avancer ; alors ce n'est pas mon cas comme je le disais là, puisque moi au contraire j'ai pu et je dis merci à la vie, je lui dis merci, je chante la vie, je danse la vie, je ne suis qu'amour, et finalement quand beaucoup de gens aujourd'hui me disent : Mais comment fais-tu pour avoir cette humanité ? Et bah je leur réponds très simplement, je leur dis : c'est ce goût de l'amour, ce goût donc qui m'a poussé, aujourd'hui, à entreprendre une construction mécanique mais demain, qui sait, peut-être, simplement à me mettre au service de la communauté, à faire le don, le don de soi...");
                            } else if (msg.equalsIgnoreCase("pierre")) {
                                event.getChat().sendMessage("présent");
                            } else if (msg.equalsIgnoreCase("manu")) {
                                event.getChat().sendMessage("EEH TU DESCEND !!");
                            } else if (msg.equalsIgnoreCase("vous conaissez ma femme")) {
                                event.getChat().sendMessage("oui chef");
                            } else if (msg.equalsIgnoreCase("animal")) {
                                event.getChat().sendMessage("poulemoute");
                            } else if (msg.equalsIgnoreCase("coca-cola")) {
                                event.getChat().sendMessage("cokaine");
                            } else if (msg.equalsIgnoreCase("coke")) {
                                event.getChat().sendMessage("coca-cola");
                            } else if (msg.equalsIgnoreCase("Francky Vincent")) {
                                event.getChat().sendMessage("Tu veux mon serveur, oui oui oui, Jvais te le donner oui oui oui");
                            } else if (msg.contains("Epicube")) {
                                event.getChat().sendMessage("Pourquoi tu dis de la merde ?");
                            } else if (msg.equalsIgnoreCase("gui")) {
                                event.getChat().sendMessage("nan c'est plus pierre");
                            }
                            else if (msg.equalsIgnoreCase("register")) {
                                int register = UUIDName.uuidTable.size();
                                event.getChat().sendMessage(Integer.toString(register)+" register");
                            } else if (msg.equalsIgnoreCase("online")) {
                                int online = 0;
                                if (!ProxyServer.getInstance().getPlayers().isEmpty()) { online = ProxyServer.getInstance().getPlayers().size();}
                                event.getChat().sendMessage(Integer.toString(online)+" online");
                            }
                            else if (msg.startsWith("msg")) {
                                String[] text = msg.split(" ");
                                if (text.length > 2) {
                                    String player = text[1];
                                    String message = msg.replaceFirst("msg ", "").replaceFirst(player+" ", "");
                                    if (ProxyServer.getInstance().getPlayer(player) != null) {
                                        ProxyServer.getInstance().getPlayer(player).sendMessage(new TextComponent("[MP] SERVER : "+message));
                                    } else {
                                        event.getChat().sendMessage("player "+player+" is not connected");
                                    }
                                } else {
                                    event.getChat().sendMessage("bad arguments (msg [player] [message])");
                                }

                            }


                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });
            }
        });

    }


}
