package com.masterpik.bungeeutil.confy;

import com.masterpik.bungeeutil.Main;
import net.md_5.bungee.config.Configuration;
import net.md_5.bungee.config.ConfigurationProvider;
import net.md_5.bungee.config.YamlConfiguration;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Array;
import java.util.ArrayList;

public class GeneralConfy {

    public static Configuration generalConfy;
    public static File generalFile;


    public static void GeneralInit() {
        try {
            if (!Main.plugin.getDataFolder().exists()) {
                Main.plugin.getDataFolder().mkdir();
            }

            generalFile = new File(Main.plugin.getDataFolder().getPath(), "general.yml");

            if (!generalFile.exists()) {
                generalFile.createNewFile();
                generalConfy = ConfigurationProvider.getProvider(YamlConfiguration.class).load(generalFile);

                ArrayList<String> motd = new ArrayList<String>();
                motd.add("Line1 MOTD");
                motd.add("Line2 MOTD");
                generalConfy.set("General.MOTD", motd);


                generalConfy.set("General.TabList.Interval", 5);

                ArrayList<String> tabHeader = new ArrayList<String>();
                tabHeader.add("Line1 tabHeader");
                tabHeader.add("Line2 tabHeader");
                generalConfy.set("General.TabList.Header", tabHeader);

                ArrayList<String> tabFooter = new ArrayList<String>();
                tabFooter.add("Line1 tabFooter");
                tabFooter.add("Line2 tabFooter");
                generalConfy.set("General.TabList.Footer", tabFooter);


                generalConfy.set("General.protocol.version", 107);
                generalConfy.set("General.protocol.name", "1.9");

                GeneralConfy.saveFile(generalConfy, generalFile);
            }
            else {
                generalConfy = ConfigurationProvider.getProvider(YamlConfiguration.class).load(generalFile);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public static ArrayList<String> getMOTD() {
        return (ArrayList<String>) generalConfy.getList("General.MOTD");
    }

    public static long getTabListInterval() {
        return generalConfy.getLong("General.TabList.Interval");
    }

    public static ArrayList<String> getTabListHeader() {
        return (ArrayList<String>) generalConfy.getList("General.TabList.Header");
    }

    public static ArrayList<String> getTabListFooter() {
        return (ArrayList<String>) generalConfy.getList("General.TabList.Footer");
    }

    public static ArrayList<Integer> getProtocolVersion() {
        return (ArrayList<Integer>) generalConfy.getIntList("General.protocol.version");
    }

    public static String getProtocolName() {
        return generalConfy.getString("General.protocol.name");
    }

    public static void saveFile(Configuration file, File fileg) {
        try {
            ConfigurationProvider.getProvider(YamlConfiguration.class).save(file, fileg);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void reload() {
        try {
            generalConfy = ConfigurationProvider.getProvider(YamlConfiguration.class).load(generalFile);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
