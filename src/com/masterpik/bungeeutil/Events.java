package com.masterpik.bungeeutil;

import com.google.common.io.ByteArrayDataInput;
import com.google.common.io.ByteStreams;
import com.masterpik.api.skype.SkypeManager;
import com.masterpik.api.util.UtilNetwork;
import com.masterpik.api.util.UtilString;
import com.masterpik.bungeeutil.commands.AdminSounds;
import com.masterpik.bungeeutil.confy.GeneralConfy;
import com.masterpik.messages.bungee.Api;
import com.samczsun.skype4j.exceptions.ConnectionException;
import net.md_5.bungee.BungeeCord;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.ServerPing;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.config.ServerInfo;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.*;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;
import net.md_5.bungee.event.EventPriority;
import net.md_5.bungee.protocol.packet.BossBar;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

public class Events implements Listener {

    @EventHandler()
    public void onPing(ProxyPingEvent event) {
        event.getResponse().setDescription(Util.motdListToMotd(GeneralConfy.getMOTD()));
        //event.getResponse().setPlayers(new ServerPing.Players());
        //event.getResponse().setVersion(new ServerPing.Protocol());
        String ip = event.getConnection().getAddress().toString().split(":")[0];
        ip = ip.replaceAll("/", "");
        /*ProxyServer.getInstance().getLogger().info(Long.toString(UtilNetwork.ip2longCompressed(ip)));*/
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void ServerConnectEvent(ServerConnectEvent event) {

        ProxiedPlayer player = event.getPlayer();

        if (event.getPlayer().getServer() == null) {

            //player.unsafe().sendPacket(new BossBar(player.getUniqueId(), 1));

            try {
                Main.chat.sendMessage("Connexion Joueur : "+player.getName());
            } catch (ConnectionException e) {
                e.printStackTrace();
            }

            for (ProxiedPlayer pl : AdminSounds.soundsPreventsList) {
                if (pl.isConnected()) {
                    if (pl != player) {
                        pl.chat("/soundplay " + pl.getName() + " BLOCK_NOTE_PLING 1 0.1");
                        pl.sendMessage(new TextComponent("§ale joueur §r"+player.getName()+" §aest co, accueil le"));
                        //BungeePackets.playSound(pl, SoundEffect.RANDOM_LEVELUP, 1F, 1F);
                    }
                }
            }
        } else {
            for (ProxiedPlayer pl : AdminSounds.soundsPreventsList) {
                if (pl.isConnected()) {
                    if (pl != player) {
                        pl.sendMessage(new TextComponent("§ble joueur §r"+player.getName()+" §best allé sur le serveur,§r "+event.getTarget().getName()));
                        //BungeePackets.playSound(pl, SoundEffect.RANDOM_LEVELUP, 1F, 1F);
                    }
                }
            }
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void PlayerQuitEvent(PlayerDisconnectEvent event) {
        if (AdminSounds.soundsPreventsList.contains(event.getPlayer())) {
            AdminSounds.soundsPreventsList.remove(event.getPlayer());
        }

        ProxiedPlayer player = event.getPlayer();


        try {
            Main.chat.sendMessage("Déconnexion Joueur : "+player.getName());
        } catch (ConnectionException e) {
            e.printStackTrace();
        }

        for (ProxiedPlayer pl : AdminSounds.soundsPreventsList) {
            if (pl.isConnected()) {
                if (pl != player) {
                    //pl.chat("/soundplay " + pl.getName() + " BLOCK_NOTE_PLING 1 0.1");
                    pl.sendMessage(new TextComponent("§cle joueur §r"+player.getName()+" §cest deco, chevre"));
                    //BungeePackets.playSound(pl, SoundEffect.RANDOM_LEVELUP, 1F, 1F);
                }
            }
        }
    }

    @EventHandler
    public void PreLoginEvent(PreLoginEvent event ) {
        if (!GeneralConfy.getProtocolVersion().contains(event.getConnection().getVersion())) {
            event.getConnection().disconnect(new TextComponent(Main.outDateKick));
            event.setCancelReason(Main.outDateKick);
            event.setCancelled(true);
        }

        //ProxyServer.getInstance().getLogger().info("Player version : "+event.getConnection().getVersion()+" | server version : "+GeneralConfy.getProtocolVersion());
    }




}
