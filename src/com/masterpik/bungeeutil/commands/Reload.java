package com.masterpik.bungeeutil.commands;

import com.masterpik.bungeeutil.TabListManager;
import com.masterpik.bungeeutil.confy.GeneralConfy;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;
public class Reload extends Command {
    public Reload(String name) {
        super(name);
    }

    @Override
    public void execute(CommandSender sender, String[] strings) {

        if (!(sender instanceof ProxiedPlayer)) {
            GeneralConfy.reload();

            TabListManager.stopTabMovement();
            TabListManager.startTabMovement();
        }

    }
}

