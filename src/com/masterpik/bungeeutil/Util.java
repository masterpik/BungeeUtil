package com.masterpik.bungeeutil;

import com.masterpik.api.util.UtilArrayList;
import net.md_5.bungee.BungeeCord;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.log.BungeeLogger;

import java.lang.reflect.Proxy;
import java.util.ArrayList;
import java.util.logging.Filter;
import java.util.logging.LogRecord;
import java.util.logging.Logger;

public class Util {

    public static String motdListToMotd(ArrayList<String> listMotd) {
        String motd = "";

        motd = listMotd.get(0) + "\n" + listMotd.get(1);
        motd = ChatColor.translateAlternateColorCodes('&', motd);

        return motd;
    }

    public static void blockLogFromServerIp(Logger log) {

        ProxyServer.getInstance().getLogger().info("enable block log start");

        Filter filter = new Filter() {
            @Override
            public boolean isLoggable(LogRecord record) {

                if (record.getMessage().contains("has connected")) {
                    String msg = record.getParameters()[0].toString();
                    if (msg.contains("/") && msg.contains(":") && msg.contains("InitialHandler")) {
                    /*ProxyServer.getInstance().getLogger().info("connection ping");

                    String msg = record.getMessage();
                    msg = msg.replaceAll("has connected", "(key)");
                    String msg2 = record.getParameters()[0].toString();
                    msg2 = msg2.replaceAll("has connected", "(key)");
                    ProxyServer.getInstance().getLogger().info("msg : "+msg);
                    ProxyServer.getInstance().getLogger().info("msg2 : "+msg2);*/

                        return false;
                    }
                }

                return true;

            }
        };

        BungeeLogger.getGlobal().setFilter(filter);
        BungeeCord.getInstance().getLogger().setFilter(filter);
        Logger.getLogger("BungeeCord").setFilter(filter);
        ProxyServer.getInstance().getLogger().setFilter(filter);
        log.setFilter(filter);


        ProxyServer.getInstance().getLogger().info("enable block log end");

    }


}
