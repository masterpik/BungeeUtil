package com.masterpik.bungeeutil;

import com.masterpik.bungeeutil.confy.GeneralConfy;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.scheduler.ScheduledTask;

import java.util.ArrayList;
import java.util.Collections;
import java.util.concurrent.TimeUnit;

public class TabListManager {

    public static ScheduledTask tabTask;

    public static void startTabMovement() {
        final int[] headerCount = {0};
        final int[] footerCount = {0};

        ArrayList<String> headerList = GeneralConfy.getTabListHeader();
        Collections.shuffle(headerList);
        ArrayList<String> footerList = GeneralConfy.getTabListFooter();
        Collections.shuffle(footerList);

        tabTask = ProxyServer.getInstance().getScheduler().schedule(Main.plugin, new Runnable() {
            @Override
            public void run() {

                ArrayList<ProxiedPlayer> players = new ArrayList<ProxiedPlayer>();
                players.addAll(ProxyServer.getInstance().getPlayers());

                int bucle = 0;
                while (bucle < players.size()) {
                    players.get(bucle).setTabHeader(new ComponentBuilder(ChatColor.translateAlternateColorCodes('&', headerList.get(headerCount[0]))).create(), new ComponentBuilder(ChatColor.translateAlternateColorCodes('&', footerList.get(footerCount[0]))).create());
                    bucle++;
                }



                if (headerCount[0] < headerList.size()-1) {
                    headerCount[0]++;
                } else {
                    headerCount[0] = 0;
                }

                if (footerCount[0] < footerList.size()-1) {
                    footerCount[0]++;
                } else {
                    footerCount[0] = 0;
                }
            }
        }, 1, GeneralConfy.getTabListInterval(), TimeUnit.MILLISECONDS);




    }

    public static void stopTabMovement() {
        tabTask.cancel();
    }

}
