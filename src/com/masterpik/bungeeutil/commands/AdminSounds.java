package com.masterpik.bungeeutil.commands;

import com.masterpik.bungeeutil.Main;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

import java.util.ArrayList;

public class AdminSounds extends Command {

    public static ArrayList<ProxiedPlayer> soundsPreventsList;

    public static void adminSoundsInit() {
        soundsPreventsList = new ArrayList<>();
        ProxyServer.getInstance().getPluginManager().registerCommand(Main.plugin, new AdminSounds("cosound"));
        ProxyServer.getInstance().getPluginManager().registerCommand(Main.plugin, new AdminSounds("cos"));
    }

    public AdminSounds(String name) {
        super(name);
    }

    @Override
    public void execute(CommandSender sender, String[] strings) {
        if (sender instanceof ProxiedPlayer) {
            ProxiedPlayer player = (ProxiedPlayer) sender;
            if (soundsPreventsList.contains(player)) {
                soundsPreventsList.remove(player);
                player.sendMessage(new TextComponent("§c§oPlay Sound On Connect §4§lOFF"));
            } else {
                soundsPreventsList.add((ProxiedPlayer) sender);
                player.sendMessage(new TextComponent("§a§oPlay Sound On Connect §2§lON"));
            }
        }
    }
}
